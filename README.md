# pkcs11-a3-utils

Lista dados de Certificado Digital A3 através do Java.

## Utilização

Criar arquivo `token.cfg` dentro do diretório que está o `.jar`.

Usar arquivos base auxiliares:

- `eToken-exemplo.cfg`
- `SmartCard-exemplo.cfg`
