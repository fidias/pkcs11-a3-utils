package lib.fidias.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author atila
 */
public class Log {

    /**
     * The log file size is set to 1MB.
     */
    private static final int FILE_SIZE = 1048576;

    private Log() {
    }

    private static Logger factory(String className) {
        Logger logger = Logger.getLogger(className);
        try {
            FileHandler fileHandler = new FileHandler("pegasus-%u.%g.log", FILE_SIZE, 5, true);
            fileHandler.setLevel(Level.INFO);
            Formatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            logger.addHandler(fileHandler);

            /*ConsoleHandler consoleHandler = new ConsoleHandler();
            consoleHandler.setLevel(Level.FINE);
            logger.addHandler(consoleHandler);*/
        } catch (IOException | SecurityException e) {
            logger.log(Level.SEVERE, null, e);
        }
        return logger;
    }

    private static class LogHolder {

        static final Logger LOG = factory(Log.class.getName());
    }

    public static Logger getLogger() {
        return LogHolder.LOG;
    }
}
