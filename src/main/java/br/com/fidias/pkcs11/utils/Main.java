package br.com.fidias.pkcs11.utils;

import java.io.Console;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.ProviderException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.fidias.log.Log;

/**
 *
 * @author atila
 */
public class Main {

    private static final Logger LOG = Log.getLogger();
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static void main(String[] args) {
        try {
            LOG.info("pkcs11-a3-utils");
            Console console = System.console();
            if (console == null) {
                LOG.warning("Não foi possível obter instância de Console");
                System.exit(0);
            }
            char[] passwordArray = console.readPassword("Senha do certificado: ");
            String senhaDoCertificadoDoCliente = String.valueOf(passwordArray);

            String fileCfg = "token.cfg";
            Provider p = new sun.security.pkcs11.SunPKCS11(fileCfg);
            Security.addProvider(p);
            char[] pin = senhaDoCertificadoDoCliente.toCharArray();
            KeyStore keystore = KeyStore.getInstance("pkcs11", p);
            keystore.load(null, pin);

            Enumeration<String> eAliases = keystore.aliases();

            while (eAliases.hasMoreElements()) {
                String alias = (String) eAliases.nextElement();
                Certificate certificado = (Certificate) keystore.getCertificate(alias);

                LOG.log(Level.INFO, "Aliais: {0}", alias);
                X509Certificate cert = (X509Certificate) certificado;

                LOG.info(cert.getSubjectDN().getName());
                LOG.log(Level.INFO, "Válido a partir de..: {0}", DATE_FORMAT.format(cert.getNotBefore()));
                LOG.log(Level.INFO, "Válido até..........: {0}", DATE_FORMAT.format(cert.getNotAfter()));
            }
        } catch (ProviderException | IOException | KeyStoreException
                | NoSuchAlgorithmException | CertificateException e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }
}
